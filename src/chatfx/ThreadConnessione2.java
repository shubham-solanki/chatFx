/*
CLASSE DI PROVA NON FUNZIONANTE NEL PROGRAMMA
 */
package chatfx;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

/**
 *
 * @author Stefano
 */
public class ThreadConnessione2 extends Thread {
    
    private ServerSocket serverSocket;
    private Socket socketClient;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private boolean isServer;
    private Consumer<Serializable> messaggioInArrivo;
    private String getIp;
    private int getPort;

    public ThreadConnessione2(boolean isServer, Consumer<Serializable> messaggioInArrivo, int getPort, String getIp) {
       
    	serverSocket = null;
        socketClient = null;
        in = null;
        out = null;
        this.isServer = isServer;
        this.messaggioInArrivo = messaggioInArrivo;
        this.getIp = getIp;
        this.getPort = getPort;
        
        
    }

    @Override
    public void run() {
        try {
            if (isServer) {
                System.out.println("Creazione Server");
                serverSocket = new ServerSocket(getPort);
                System.out.println("In attesa di una connessione del client");
                socketClient = serverSocket.accept();
                System.out.println("Client connesso");
            } else {
                System.out.println("Creazione Client in corso");
                socketClient = new Socket(getIp, getPort);
                System.out.println("Client creato e connesso al server");

            }
            in = new ObjectInputStream(socketClient.getInputStream());
            out = new ObjectOutputStream(socketClient.getOutputStream());
            out.flush();
            // per non aver problemi con la connessione
            socketClient.setTcpNoDelay(true);

            while(true) {
                Serializable data = (Serializable) in.readObject();
                messaggioInArrivo.accept(data);
            }

        } catch (Exception e){
            messaggioInArrivo.accept("Connessione Chiusa");
        }
    } 
    
    public void closeConnection() throws IOException {
        serverSocket.close();
        socketClient.close();
        in.close();
        out.close();
    }
    
     public void send(Serializable data) throws Exception {
        out.writeObject(data);
    }
}
